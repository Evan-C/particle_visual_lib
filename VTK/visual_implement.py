from __future__ import print_function
import vtk
import numpy as np

points=vtk.vtkPoints()
data=np.array([])
dataPath = ""

# Read data from disk
def GetData(path):
    # with np.loadtxt(path, dtype=np.float32) as data:
    #     return data
    try:
        return np.loadtxt(path, dtype=np.float32)
    except:
        print("Failed to open data file")
        exit(-3)


class vtkTimerCallback():
   def __init__(self):
       self.timer_count = 0

   def execute(self, obj, event):
       print(self.timer_count)
       global data
       global points
       # data = GetData(dataPath)
       data += 0.001
       points.Reset()
       for i in range(0, data.shape[0]):
           points.InsertNextPoint(data[i, 0], data[i, 1], data[i, 2])
           points.InsertNextPoint(data[i, 3], data[i, 4], data[i, 5])
       iren = obj
       iren.GetRenderWindow().Render()
       self.timer_count += 1


def visual_partical():
    global data
    global points
# Read data from File
    dataPath = "water_path.txt"
    data = GetData(dataPath)


# create points and lines
    for i in range(0, data.shape[0]):
        points.InsertNextPoint(data[i, 0], data[i, 1], data[i, 2])
        points.InsertNextPoint(data[i, 3], data[i, 4], data[i, 5])

    # Create a cell array to store the lines in and add the lines to it
    lines = vtk.vtkCellArray()

    for i in range(0, data.shape[0]*2, 2):
        line = vtk.vtkLine()
        line.GetPointIds().SetId(0, i)
        line.GetPointIds().SetId(1, i + 1)
        lines.InsertNextCell(line)

# Create a mapper and actor
    # Create a polydata to store everything in
    linesPolyData = vtk.vtkPolyData()
    # Add the points to the dataset
    linesPolyData.SetPoints(points)
    # Add the lines to the dataset
    linesPolyData.SetLines(lines)
    # Setup actor and mapper
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(linesPolyData)
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    prop = actor.GetProperty()
    prop.SetLineWidth(0.1)
    colors = vtk.vtkNamedColors()
    prop.SetColor(colors.GetColor3d("Blue"))


# Setup a renderer, render window, and interactor
    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
# renderWindow.SetWindowName("Test")

    renderWindow.AddRenderer(renderer)
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

# Add the actor to the scene
    renderer.AddActor(actor)
    renderer.SetBackground(1, 1, 1)  # Background color white

# Render and interact
    renderWindow.SetWindowName("Line")
    renderWindow.SetSize(1600, 900)
    renderWindow.Render()

# Initialize must be called prior to creating timer events.
    renderWindowInteractor.Initialize()

# Sign up to receive TimerEvent
    cb = vtkTimerCallback()
    cb.actor = actor
    renderWindowInteractor.AddObserver('TimerEvent', cb.execute)
    timerId = renderWindowInteractor.CreateRepeatingTimer(100)

# start the interaction and timer
    renderWindowInteractor.Start()


def main():
    visual_partical()


if __name__ == '__main__':
    main()


