from __future__ import print_function
import vtk
import numpy as np
from VTK import get_conf

points = vtk.vtkPoints()
data = np.array([])
dataPath = ""
flag = 0
conf = get_conf.configure("config.ini")

# Read data from disk
def GetData(path):
    try:
        return np.loadtxt(path, dtype=np.float32)
    except:
        print("Failed to open data file")
        exit(-3)


class vtkTimerCallback():
   def __init__(self):
       self.timer_count = 0

   def execute(self, obj, event):
        global data
        global points
        global flag
        global conf
        data += 0.001
        # check read flag
        flag = conf.GetFlag()
        conf.IfExit()
        if flag == 0:
            print(self.timer_count)
            # reset points
            points.Reset()
            # refresh points
            for i in range(0, data.shape[0]):
               points.InsertNextPoint(data[i, 0], data[i, 1], data[i, 2])
               points.InsertNextPoint(data[i, 3], data[i, 4], data[i, 5])
            iren = obj
            # rerender window
            iren.GetRenderWindow().Render()
            self.timer_count += 1
            # change file read flag
            conf.ChangeFlag()
        else:
            None

def CallRerender():
    data += 0.001
    points.Reset()
    for i in range(0, data.shape[0]):
        points.InsertNextPoint(data[i, 0], data[i, 1], data[i, 2])
        points.InsertNextPoint(data[i, 3], data[i, 4], data[i, 5])

def visual_partical():
    global data
    global points
# Read data from File
    dataPath = "water_path.txt"
    data = GetData(dataPath)

    colors = vtk.vtkNamedColors()



    max_x2 = data[:, 3].max()
    max_y2 = data[:, 4].max()
    max_z2 = data[:, 5].max()

    min_x2 = data[:, 3].min()
    min_y2 = data[:, 4].min()
    min_z2 = data[:, 5].min()

    # create cube edge
    edge = vtk.vtkPoints()
    edge.InsertNextPoint(min_x2, min_y2, min_z2)
    edge.InsertNextPoint(min_x2, min_y2, max_z2)
    edge.InsertNextPoint(max_x2, min_y2, max_z2)
    edge.InsertNextPoint(max_x2, max_y2, max_z2)
    edge.InsertNextPoint(min_x2, max_y2, max_z2)
    #
    edge.InsertNextPoint(min_x2, max_y2, max_z2)
    edge.InsertNextPoint(min_x2, max_y2, min_z2)
    edge.InsertNextPoint(max_x2, max_y2, min_z2)
    edge.InsertNextPoint(max_x2, max_y2, min_z2)
    edge.InsertNextPoint(max_x2, min_y2, min_z2)
    edge.InsertNextPoint(min_x2, min_y2, min_z2)
    #
    edge.InsertNextPoint(min_x2, max_y2, min_z2)
    edge.InsertNextPoint(max_x2, max_y2, min_z2)
    edge.InsertNextPoint(max_x2, max_y2, max_z2)
    #
    edge.InsertNextPoint(max_x2, min_y2, max_z2)
    edge.InsertNextPoint(max_x2, min_y2, min_z2)
    #
    edge.InsertNextPoint(min_x2, min_y2, min_z2)
    edge.InsertNextPoint(min_x2, min_y2, max_z2)
    edge.InsertNextPoint(min_x2, max_y2, max_z2)

    line = vtk.vtkLineSource()
    line.SetPoints(edge)
    line.SetResolution(32)

    mapperl = vtk.vtkPolyDataMapper()
    mapperl.SetInputConnection(line.GetOutputPort())

    actorl = vtk.vtkActor()
    actorl.SetMapper(mapperl)
    propl = actorl.GetProperty()
    propl.SetColor(0.75, 0.1, 0.1)
    propl.SetLineWidth(2)
    # actorl.GetProperty().SetColor(1.5, 0., 0.)



# create points and lines
    for i in range(0, data.shape[0]):
        points.InsertNextPoint(data[i, 0], data[i, 1], data[i, 2])
        points.InsertNextPoint(data[i, 3], data[i, 4], data[i, 5])

    # Create a cell array to store the lines in and add the lines to it
    lines = vtk.vtkCellArray()

    for i in range(0, data.shape[0]*2, 2):
        line = vtk.vtkLine()
        line.GetPointIds().SetId(0, i)
        line.GetPointIds().SetId(1, i + 1)
        lines.InsertNextCell(line)

# Create a mapper and actor
    # Create a polydata to store everything in
    linesPolyData = vtk.vtkPolyData()
    # Add the points to the dataset
    linesPolyData.SetPoints(points)
    # Add the lines to the dataset
    linesPolyData.SetLines(lines)
    # Setup actor and mapper
    mapper = vtk.vtkPolyDataMapper()
    mapper.SetInputData(linesPolyData)
    actor = vtk.vtkActor()
    actor.SetMapper(mapper)
    prop = actor.GetProperty()
    prop.SetLineWidth(0.1)
    # prop.SetColor(colors.GetColor3d("Blue"))
    prop.SetColor(0.1, 0.1, 0.8)


# Setup a renderer, render window, and interactor
    renderer = vtk.vtkRenderer()
    renderWindow = vtk.vtkRenderWindow()
# renderWindow.SetWindowName("Test")

    renderWindow.AddRenderer(renderer)
    renderWindowInteractor = vtk.vtkRenderWindowInteractor()
    renderWindowInteractor.SetRenderWindow(renderWindow)

# Add the actor to the scene
    renderer.AddActor(actor)
    renderer.AddActor(actorl)
    renderer.SetBackground(1, 1, 1)  # Background color white


    camera = vtk.vtkCamera()
    # Camera Postion
    camera.SetPosition(0.06, -0.083, 0.03)
    # Camera intrest Point
    camera.SetFocalPoint(0.02, 0.02, 0)
    # FOV
    camera.SetEyeAngle(85)
    renderer.SetActiveCamera(camera)

# Render and interact
    renderWindow.SetWindowName("Line")
    renderWindow.SetSize(1280, 720)
    renderWindow.Render()

# Initialize must be called prior to creating timer events.
    renderWindowInteractor.Initialize()

# Sign up to receive TimerEvent
    cb = vtkTimerCallback()
    cb.actor = actor
    renderWindowInteractor.AddObserver('TimerEvent', cb.execute)
    timerId = renderWindowInteractor.CreateRepeatingTimer(200)

# start the interaction and timer
    renderWindowInteractor.Start()


def main():
    global flag
    global dataPath
    conf = get_conf.configure("config.ini")
    flag = conf.GetFlag()
    dataPath = conf.GetdatdaPath()
    visual_partical()


if __name__ == '__main__':
    main()


