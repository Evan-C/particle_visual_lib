from configparser import ConfigParser
import sys

class configure:
    confPath = ""
    flag = 1
    dataPath = ""

    def __init__(self, confPath):
        self.confPath = confPath


    def GetdatdaPath(self):
        cfg = ConfigParser()
        cfg.read(self.confPath)
        return cfg.get('configure', 'dataPath')

    def GetFlag(self):
        cfg = ConfigParser()
        cfg.read(self.confPath)
        self.flag = cfg.getint('configure', 'flag')
        return self.flag

    def ChangeFlag(self):
        cfg = ConfigParser()
        cfg.read(self.confPath)
        # if self.flag == 0:
        #     cfg.set('configure', 'flag', "1")
        # else:
        #     cfg.set('configure', 'flag', "0")
        cfg.set('configure', 'flag', "1")
        with open(self.confPath, "w+") as f:
            cfg.write(f)

    def IfExit(self):
        cfg = ConfigParser()
        cfg.read(self.confPath)
        endFlag = cfg.get('configure', 'endFlag')
        if endFlag == "end":
            exit(0)





